# Taltavle

Dette er en taltavle til undervisning og træning i positionssystemet (begrænset til 1'ere og 10'ere);

Find tallets placering i taltavlen - når taltavlen er helt fyldt, bliver den hver gang gradvist mindre befolket og dermed sværere.

Reload på den røde knap, for at starte tavlen forfra, på opnåede sværhedsgrad.

Reload browseren for at nulstille tavle og sværhedsgrad.

Markér og tæl med den gule markeringsfarve - tallet bliver grønt, når man finder den rigtige placering.

